﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Recipes.Domain.RecipeSteps;

namespace Recipes.Data.Configurations
{
    public class RecipeStepConfiguration : IEntityTypeConfiguration<RecipeStep>
    {
        public void Configure(EntityTypeBuilder<RecipeStep> builder)
        {
            builder.HasOne(rs => rs.Recipe)
                 .WithMany(r => r.RecipeSteps)
                 .HasForeignKey(rs => rs.RecipeID);

            builder.Property(rs => rs.Instruction)
                .HasMaxLength(500)
                .IsRequired();

            builder.Property(rs => rs.ImageUrl)
                .HasMaxLength(250);
        }
    }
}
