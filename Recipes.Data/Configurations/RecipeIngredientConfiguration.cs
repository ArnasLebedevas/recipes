﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Recipes.Domain.RecipeIngredients;

namespace Recipes.Data.Configurations
{
    public class RecipeIngredientConfiguration : IEntityTypeConfiguration<RecipeIngredient>
    {
        public void Configure(EntityTypeBuilder<RecipeIngredient> builder)
        {
            builder.HasKey(ri => new { ri.RecipeID, ri.IngredientID });

            builder.HasOne(ri => ri.Recipe)
               .WithMany(r => r.RecipeIngredients)
               .HasForeignKey(ri => ri.RecipeID);

            builder.HasOne(ri => ri.Ingredient)
               .WithMany(r => r.RecipeIngredients)
               .HasForeignKey(ri => ri.IngredientID);
        }
    }
}
