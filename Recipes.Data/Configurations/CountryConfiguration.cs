﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Recipes.Domain.Countries;

namespace Recipes.Data.Configurations
{
    public class CountryConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.HasMany(a => a.Recipes)
                .WithOne(r => r.Country)
                .HasForeignKey(r => r.CountryID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
