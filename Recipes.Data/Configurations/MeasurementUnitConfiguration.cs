﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Recipes.Domain.MeasurementUnits;

namespace Recipes.Data.Configurations
{
    public class MeasurementUnitConfiguration : IEntityTypeConfiguration<MeasurementUnit>
    {
        public void Configure(EntityTypeBuilder<MeasurementUnit> builder)
        {
            builder.HasOne(mu => mu.IngredientQuantity)
               .WithOne(iq => iq.MeasurementUnit)
               .HasForeignKey<MeasurementUnit>(mu => mu.IngredientQuantityID)
               .OnDelete(DeleteBehavior.Cascade)
               .IsRequired();
        }
    }
}
