﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Recipes.Domain.Ingredients;

namespace Recipes.Data.Configurations
{
    public class IngredientQuantityConfiguration : IEntityTypeConfiguration<IngredientQuantity>
    {
        public void Configure(EntityTypeBuilder<IngredientQuantity> builder)
        {
            builder.HasOne(iq => iq.Ingredient)
               .WithOne(i => i.IngredientQuantity)
               .HasForeignKey<IngredientQuantity>(i => i.IngredientID)
               .OnDelete(DeleteBehavior.Cascade)
               .IsRequired();

            builder.HasOne(iq => iq.MeasurementUnit)
               .WithOne(mu => mu.IngredientQuantity)
               .HasForeignKey<IngredientQuantity>(iq => iq.MeasurementUnitID)
               .OnDelete(DeleteBehavior.Cascade)
               .IsRequired();
        }
    }
}
