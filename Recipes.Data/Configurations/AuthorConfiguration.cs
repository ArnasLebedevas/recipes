﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Recipes.Domain.Authors;

namespace Recipes.Data.Configurations
{
    public class AuthorConfiguration : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder.HasMany(a => a.Recipes)
                .WithOne(r => r.Author)
                .HasForeignKey(r => r.AuthorID)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(a => new {
                a.Name,
                a.Surname,
                a.Email
            });

            builder.HasAlternateKey(a => new {
                a.Name,
                a.Surname,
                a.Email
            });

            builder.Property(a => a.Name)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(a => a.Surname)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(a => a.Email)
                .HasMaxLength(60)
                .IsRequired();

            builder.Property(a => a.ImageUrl)
                .HasMaxLength(250);
        }
    }
}
