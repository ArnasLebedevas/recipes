﻿using Microsoft.EntityFrameworkCore;
using Recipes.Data.Configurations;
using Recipes.Domain.Authors;
using Recipes.Domain.Countries;
using Recipes.Domain.Ingredients;
using Recipes.Domain.Recipes;

namespace Recipes.Data.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        { }

        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AuthorConfiguration());
            builder.ApplyConfiguration(new RecipeStepConfiguration());
            builder.ApplyConfiguration(new CountryConfiguration());
            builder.ApplyConfiguration(new RecipeIngredientConfiguration());
            builder.ApplyConfiguration(new IngredientQuantityConfiguration());
            builder.ApplyConfiguration(new MeasurementUnitConfiguration());
        }
    }
}
