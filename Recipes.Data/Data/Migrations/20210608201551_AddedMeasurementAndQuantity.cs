﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Recipes.Data.Migrations
{
    public partial class AddedMeasurementAndQuantity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IngredientQuantityID",
                table: "Ingredients",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "IngredientQuantity",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Amount = table.Column<float>(type: "REAL", nullable: false),
                    MeasurementUnitID = table.Column<int>(type: "INTEGER", nullable: false),
                    IngredientID = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IngredientQuantity", x => x.ID);
                    table.ForeignKey(
                        name: "FK_IngredientQuantity_Ingredients_IngredientID",
                        column: x => x.IngredientID,
                        principalTable: "Ingredients",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MeasurementUnit",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Unit = table.Column<string>(type: "TEXT", nullable: true),
                    IngredientQuantityID = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeasurementUnit", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MeasurementUnit_IngredientQuantity_IngredientQuantityID",
                        column: x => x.IngredientQuantityID,
                        principalTable: "IngredientQuantity",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IngredientQuantity_IngredientID",
                table: "IngredientQuantity",
                column: "IngredientID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MeasurementUnit_IngredientQuantityID",
                table: "MeasurementUnit",
                column: "IngredientQuantityID",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MeasurementUnit");

            migrationBuilder.DropTable(
                name: "IngredientQuantity");

            migrationBuilder.DropColumn(
                name: "IngredientQuantityID",
                table: "Ingredients");
        }
    }
}
