﻿using System.ComponentModel.DataAnnotations;

namespace Recipes.Models.Authors
{
    public class AuthorModel : IEntityModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Please enter Author name"), MinLength(2), MaxLength(20)]
        [Display(Name = "Author name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter Author surname"), MinLength(2), MaxLength(20)]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Please enter Email")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email is not valid.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter country name"), MinLength(4), MaxLength(20)]
        public string Country { get; set; }
        [Required(ErrorMessage = "Please enter city name"), MinLength(3), MaxLength(20)]
        public string City { get; set; }
        public string ImageUrl { get; set; }
    }
}
