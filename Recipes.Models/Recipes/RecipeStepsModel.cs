﻿using System.ComponentModel.DataAnnotations;

namespace Recipes.Models.Recipes
{
    public class RecipeStepsModel : IEntityModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Please enter instruction"), MinLength(2)]
        public string Instruction { get; set; }
        public string ImageUrl { get; set; }
    }
}
