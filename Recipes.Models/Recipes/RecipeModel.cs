﻿using Recipes.Models.Authors;
using Recipes.Models.Countries;
using Recipes.Models.RecipeIngredients;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Recipes.Models.Recipes
{
    public class RecipeModel : IEntityModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage ="Please enter recipe title"), MaxLength(50)]
        public string Title { get; set; }
        [Required(ErrorMessage = "Please enter recipe description")]
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        [Required]
        [Display(Name = "Preparation time (minutes)")]
        public int PreparationTimeInMinutes { get; set; }
        [Required]
        [Display(Name = "Total time (minutes)")]
        public int TotalTimeInMinutes { get; set; }
        public int AuthorID { get; set; }
        public AuthorModel Author { get; set; }
        public int CountryID { get; set; }
        public CountryModel Country { get; set; }
        public List<RecipeStepsModel> RecipeSteps { get; set; }
        public List<RecipeIngredientModel> RecipeIngredients { get; set; }
    }
}
