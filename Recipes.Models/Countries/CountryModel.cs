﻿using System.ComponentModel.DataAnnotations;

namespace Recipes.Models.Countries
{
    public class CountryModel : IEntityModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Please enter country name"), MinLength(3), MaxLength(20)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter capital name"), MinLength(3), MaxLength(20)]
        public string Capital { get; set; }
        [Required(ErrorMessage = "Please enter continent name"), MinLength(3), MaxLength(20)]
        public string Continent { get; set; }
        [Required(ErrorMessage = "Please enter language name"), MinLength(3), MaxLength(20)]
        public string Language { get; set; }
        [Required(ErrorMessage = "Please enter culture name"), MinLength(3), MaxLength(50)]
        public string Culture { get; set; }
    }
}
