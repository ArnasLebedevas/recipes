﻿namespace Recipes.Models.RecipeIngredients
{
    public class IngredientQuantityModel
    {
        public float Amount { get; set; }
        public string MeasurementUnit { get; set; }
    }
}
