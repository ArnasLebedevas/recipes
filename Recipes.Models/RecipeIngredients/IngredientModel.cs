﻿using System.ComponentModel.DataAnnotations;

namespace Recipes.Models.RecipeIngredients
{
    public class IngredientModel
    {
        public int? ID { get; set; }
        [Required(ErrorMessage = "Please enter ingredient name"), MinLength(2), MaxLength(40)]
        public string Name { get; set; }
        public IngredientQuantityModel IngredientQuantity { get; set; }
    }
}
