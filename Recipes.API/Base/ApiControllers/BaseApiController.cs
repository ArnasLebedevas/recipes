﻿using Microsoft.AspNetCore.Mvc;
using Recipes.Data.Core;
using Recipes.Domain;
using Recipes.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recipes.API.Base.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseApiController<TEntity, TModel> : ControllerBase
        where TEntity : class, IEntity
        where TModel : class, IEntityModel
    {
        private readonly IRepository<TEntity, TModel> _repository;

        public BaseApiController(IRepository<TEntity, TModel> repository)
        {
            _repository = repository;
        }

        [HttpPost]
        public async Task<int> Create(TModel entity)
        {
            return await _repository.Add(entity);
        }

        [HttpDelete("{id}")]
        public async Task<TEntity> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        [HttpPut("{id}")]
        public async Task<TModel> Update(TModel entity)
        {
            return await _repository.Update(entity);
        }

        [HttpGet]
        public async Task<List<TModel>> GetAll()
        {
            return await _repository.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<TModel> GetAsync(int id)
        {
            return await _repository.GetAsync(id);
        }
    }
}
