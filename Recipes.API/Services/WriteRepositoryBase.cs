﻿using Microsoft.EntityFrameworkCore;
using Recipes.Data.Core.Base;
using Recipes.Data.Data;
using Recipes.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.API.Services
{
    public class WriteRepositoryBase : IWriteRepository
    {
        private readonly DataContext _context;

        public WriteRepositoryBase(DataContext context)
        {
            _context = context;
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            if (entity == null)
                return;

            _context.Set<TEntity>().Remove(entity);
        }

        public async Task<List<TEntity>> GetAllAsync<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class, new()
        {
            return await _context.Set<TEntity>().Where(where).ToListAsync();
        }

        public async Task SaveChangesAsync()
        {
            await SaveChangesAsync(CancellationToken.None);
        }

        private async Task SaveChangesAsync(CancellationToken cancellationToken)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<TEntity> FirstOrDefaultAsync<TEntity>(int id) where TEntity : class, IEntity
        {
            return await _context.Set<TEntity>().FirstOrDefaultAsync(o => o.ID == id);
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class
        {
            return _context.Set<TEntity>().AsQueryable<TEntity>();
        }

        public async Task<TEntity> FirstOrDefaultAsync<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class, new()
        {
            return await _context.Set<TEntity>().FirstOrDefaultAsync(where);
        }
    }
}
