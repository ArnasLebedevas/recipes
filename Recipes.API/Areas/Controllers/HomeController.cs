﻿using Microsoft.AspNetCore.Mvc;

namespace Recipes.API.Areas.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
