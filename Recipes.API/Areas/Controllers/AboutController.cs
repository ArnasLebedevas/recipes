﻿using Microsoft.AspNetCore.Mvc;

namespace Recipes.API.Areas.Controllers
{
    public class AboutController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
