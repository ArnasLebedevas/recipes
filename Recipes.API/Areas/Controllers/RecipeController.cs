﻿using Microsoft.AspNetCore.Mvc;
using Recipes.Data.Core;
using Recipes.Domain.Recipes;
using Recipes.Models.Recipes;
using System.Threading.Tasks;

namespace Recipes.API.Areas.Controllers
{
    public class RecipeController : Controller
    {
        private readonly IRepository<Recipe, RecipeModel> repository;

        public RecipeController(IRepository<Recipe, RecipeModel> repository)
        {
            this.repository = repository;
        }

        public async Task<IActionResult> GetAllRecipes()
        {
            var recipes = await repository.GetAllAsync();

            return View(recipes);
        }

        public async Task<IActionResult> GetRecipe(int id)
        {
            var recipe = await repository.GetAsync(id);

            return View(recipe);
        }

        public IActionResult AddRecipe()
        {
            return View();
        }
    }
}
