﻿using Recipes.API.Infrastructure.Services;
using Recipes.API.Services;
using Recipes.Data.Core;
using Recipes.Data.Core.Base;
using Recipes.Data.Core.Recipes.Repository;
using Recipes.Domain.Recipes;
using Recipes.Models.Recipes;

namespace Recipes.API.Infrastructure
{
    public class WebConfiguration: IServiceConfiguration
    {
        public void Configure(IServices services)
        {
            services.AddScoped<IRepository<Recipe, RecipeModel>, RecipeRepository>();
            services.AddScoped<IWriteRepository, WriteRepositoryBase>();
        }
    }
}
