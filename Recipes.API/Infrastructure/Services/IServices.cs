﻿namespace Recipes.API.Infrastructure.Services
{
    public interface IServices
    {
        void AddScoped<TService, TImplementation>()
            where TImplementation : class, TService
            where TService : class;
    }
}
