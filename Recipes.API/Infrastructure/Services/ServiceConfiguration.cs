﻿namespace Recipes.API.Infrastructure.Services
{
    public interface IServiceConfiguration
    {
        void Configure(IServices services);
    }

    public static class ServiceCollectionExtensions
    {
        public static void Add(this IServices services, IServiceConfiguration configuration)
        {
            configuration.Configure(services);
        }
    }
}
