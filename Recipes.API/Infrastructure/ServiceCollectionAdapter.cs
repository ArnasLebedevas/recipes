﻿using Microsoft.Extensions.DependencyInjection;
using Recipes.API.Infrastructure.Services;

namespace Recipes.API.Infrastructure
{
    public class ServiceCollectionAdapter : IServices
    {
        private readonly IServiceCollection _serviceCollection;

        public ServiceCollectionAdapter(IServiceCollection serviceCollection)
        {
            _serviceCollection = serviceCollection;
        }

        public void AddScoped<TService, TImplementation>()
           where TImplementation : class, TService
           where TService : class
        {
            _serviceCollection.AddScoped<TService, TImplementation>();
        }
    }
}
