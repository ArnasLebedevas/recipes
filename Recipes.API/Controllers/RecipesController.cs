﻿using Recipes.API.Base.ApiControllers;
using Recipes.Data.Core;
using Recipes.Domain.Recipes;
using Recipes.Models.Recipes;

namespace Recipes.API.Controllers
{
    public class RecipesController : BaseApiController<Recipe, RecipeModel>
    {
        public RecipesController(IRepository<Recipe, RecipeModel> repository) : base(repository)
        { }
    }
}
