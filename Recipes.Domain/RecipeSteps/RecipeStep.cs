﻿using Recipes.Domain.Recipes;

namespace Recipes.Domain.RecipeSteps
{
    public class RecipeStep : IEntity
    {
        public int ID { get; set; }
        public string Instruction { get; set; }
        public string ImageUrl { get; set; }
        public int RecipeID { get; set; }
        public Recipe Recipe { get; set; }
    }
}
