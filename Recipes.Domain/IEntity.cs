﻿namespace Recipes.Domain
{
    public interface IEntity
    {
        public int ID { get; set; }
    }
}
