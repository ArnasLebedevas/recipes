﻿using Recipes.Domain.Ingredients;

namespace Recipes.Domain.MeasurementUnits
{
    public class MeasurementUnit
    {
        public int ID { get; set; }
        public string Unit { get; set; }
        public int IngredientQuantityID { get; set; }
        public IngredientQuantity IngredientQuantity { get; set; }
    }
}
