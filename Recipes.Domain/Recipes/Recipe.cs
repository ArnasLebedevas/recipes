﻿using Recipes.Domain.Authors;
using Recipes.Domain.Countries;
using Recipes.Domain.RecipeIngredients;
using Recipes.Domain.RecipeSteps;
using System.Collections.Generic;

namespace Recipes.Domain.Recipes
{
    public class Recipe : IEntity
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int PreparationTimeInMinutes { get; set; }
        public int TotalTimeInMinutes { get; set; }
        public int AuthorID { get; set; }
        public Author Author { get; set; }
        public int CountryID { get; set; }
        public Country Country { get; set; }
        public ICollection<RecipeStep> RecipeSteps { get; set; }
        public ICollection<RecipeIngredient> RecipeIngredients { get; set; }

        public void AddRecipeStep(RecipeStep entity)
        {
            if (RecipeSteps == null)
                RecipeSteps = new List<RecipeStep>();

            RecipeSteps.Add(entity);
        }

        public void AddRecipeIngredient(RecipeIngredient entity)
        {
            if (RecipeIngredients == null)
                RecipeIngredients = new List<RecipeIngredient>();

            RecipeIngredients.Add(entity);
        }
    }
}
