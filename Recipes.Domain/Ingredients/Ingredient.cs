﻿using Recipes.Domain.RecipeIngredients;
using System.Collections.Generic;

namespace Recipes.Domain.Ingredients
{
    public class Ingredient : IEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int IngredientQuantityID { get; set; }
        public IngredientQuantity IngredientQuantity { get; set; }
        public ICollection<RecipeIngredient> RecipeIngredients { get; set; }
    }
}
