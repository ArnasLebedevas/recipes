﻿using Recipes.Domain.MeasurementUnits;

namespace Recipes.Domain.Ingredients
{
    public class IngredientQuantity
    {
        public int ID { get; set; }
        public float Amount { get; set; }
        public int MeasurementUnitID { get; set; }
        public MeasurementUnit MeasurementUnit { get; set; }
        public int IngredientID { get; set; }
        public Ingredient Ingredient { get; set; }
    }
}
