﻿using Recipes.Domain.Recipes;
using System.Collections.Generic;

namespace Recipes.Domain.Countries
{
    public class Country : IEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Capital { get; set; }
        public string Continent { get; set; }
        public string Language { get; set; }
        public string Culture { get; set; }
        public ICollection<Recipe> Recipes { get; set; } = new List<Recipe>();
    }
}
