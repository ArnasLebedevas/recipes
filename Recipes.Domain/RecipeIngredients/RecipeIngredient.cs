﻿using Recipes.Domain.Ingredients;
using Recipes.Domain.Recipes;

namespace Recipes.Domain.RecipeIngredients
{
    public class RecipeIngredient
    {
        public int RecipeID { get; set; }
        public Recipe Recipe { get; set; }

        public int IngredientID { get; set; }
        public Ingredient Ingredient { get; set; }
    }
}
