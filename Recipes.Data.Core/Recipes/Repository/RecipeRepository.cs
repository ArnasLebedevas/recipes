﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Recipes.Data.Core.Base;
using Recipes.Data.Data;
using Recipes.Domain.Authors;
using Recipes.Domain.Countries;
using Recipes.Domain.Ingredients;
using Recipes.Domain.MeasurementUnits;
using Recipes.Domain.RecipeIngredients;
using Recipes.Domain.Recipes;
using Recipes.Domain.RecipeSteps;
using Recipes.Models.Recipes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recipes.Data.Core.Recipes.Repository
{
    public class RecipeRepository : BaseRepository<Recipe, RecipeModel>
    {
        private readonly IMapper mapper;
        private readonly DataContext context;

        public RecipeRepository(IWriteRepository repository, DataContext context, IMapper mapper) : base(repository)
        {
            this.mapper = mapper;
            this.context = context;
        }

        public async override Task<int> Add(RecipeModel entity)
        {
            Country country = null;

            if (entity.Country.ID > 0)
                country = await _repository.FirstOrDefaultAsync<Country>(entity.Country.ID);

            if (country == null)
                country = await _repository.FirstOrDefaultAsync<Country>(c => c.Name == entity.Country.Name && c.Capital == entity.Country.Capital);
            
            if (country == null)
            {
                country = new Country();

                mapper.Map(entity.Country, country);
            }
   
            Author author = null;

            if (entity.Author.ID > 0)
                author = await _repository.FirstOrDefaultAsync<Author>(entity.Author.ID);

            if (author == null)
                author = await _repository.FirstOrDefaultAsync<Author>(c => c.Name == entity.Author.Name && c.Surname == entity.Author.Surname && c.Email == entity.Author.Email);

            if (author == null)
            {
                author = new Author();

                mapper.Map(entity.Author, author);
            }

            Recipe recipe = null;

            if (entity.ID > 0)
                recipe = await _repository.FirstOrDefaultAsync<Recipe>(entity.ID);

            if (recipe == null)
                recipe = await _repository.FirstOrDefaultAsync<Recipe>(r => r.AuthorID == author.ID && r.Title == entity.Title);

            if (recipe == null)
            {
                recipe = new Recipe();

                _repository.Add(recipe);
            }

            recipe.Title = entity.Title;
            recipe.Description = entity.Description;
            recipe.ImageUrl = entity.ImageUrl;
            recipe.PreparationTimeInMinutes = entity.PreparationTimeInMinutes;
            recipe.TotalTimeInMinutes = entity.TotalTimeInMinutes;
            recipe.Author = author;
            recipe.Country = country;

            entity.RecipeSteps.ForEach(rs =>
            {
                RecipeStep recipeStep = new RecipeStep()
                {
                    Instruction = rs.Instruction,
                    ImageUrl = rs.ImageUrl
                };

                recipe.AddRecipeStep(recipeStep);
            });

            entity.RecipeIngredients.ForEach(ri =>
            {
                Ingredient ingredient = null;
                RecipeIngredient recipeIngredient = null;

                if (ri.Ingredient.ID > 0)
                    ingredient = context.Ingredients.Find(ri.Ingredient.ID);

                if (ingredient != null)
                    recipeIngredient = new RecipeIngredient() { Ingredient = ingredient };
                else
                    recipeIngredient = new RecipeIngredient()
                    {
                        Ingredient = new Ingredient()
                        {
                            Name = ri.Ingredient.Name,
                            IngredientQuantity = new IngredientQuantity()
                            {
                                Amount = ri.Ingredient.IngredientQuantity.Amount,
                                MeasurementUnit = new MeasurementUnit()
                                {
                                    Unit = ri.Ingredient.IngredientQuantity.MeasurementUnit
                                }
                            }
                        }
                    };

                recipe.AddRecipeIngredient(recipeIngredient);
            });

            await _repository.SaveChangesAsync();

            return entity.ID;
        }

        public async override Task<Recipe> Delete(int id)
        {
            var result = await _repository.FirstOrDefaultAsync<Recipe>(id);

            if (result == null)
                return result;

            _repository.Delete(result);

            await _repository.SaveChangesAsync();

            return result;
        }

        public async override Task<List<RecipeModel>> GetAllAsync()
        {
            var recipes = await context.Recipes.Include(a => a.Author).Include(c => c.Country).Include(r => r.RecipeSteps)
                .Include(r => r.RecipeIngredients).ThenInclude(ri => ri.Ingredient).ThenInclude(iq => iq.IngredientQuantity)
                .ThenInclude(mu => mu.MeasurementUnit).ToListAsync();

            return mapper.Map<List<RecipeModel>>(recipes);
        }

        public async override Task<RecipeModel> GetAsync(int id)
        {
            return await context.Recipes.ProjectTo<RecipeModel>(mapper.ConfigurationProvider).FirstOrDefaultAsync(x => x.ID == id);
        }

        public async override Task<RecipeModel> Update(RecipeModel entity)
        {
            var result = await _repository.FirstOrDefaultAsync<Recipe>(entity.ID);

            if (result == null)
                return null;

            mapper.Map(entity, result);

            await _repository.SaveChangesAsync();

            return entity;
        }
    }
}
