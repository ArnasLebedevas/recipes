﻿using AutoMapper;
using Recipes.Domain.Authors;
using Recipes.Domain.Countries;
using Recipes.Domain.Ingredients;
using Recipes.Domain.RecipeIngredients;
using Recipes.Domain.Recipes;
using Recipes.Domain.RecipeSteps;
using Recipes.Models.Authors;
using Recipes.Models.Countries;
using Recipes.Models.RecipeIngredients;
using Recipes.Models.Recipes;

namespace Recipes.Data.Core.Recipes.Mapper
{
    public class RecipeMapper_new : Profile
    {
        public RecipeMapper_new()
        {
            //Create
            CreateMap<CountryModel, Country>()
                .ForMember(c => c.Name, o => o.MapFrom(s => s.Name))
                .ForMember(c => c.Capital, o => o.MapFrom(s => s.Capital))
                .ForMember(c => c.Continent, o => o.MapFrom(s => s.Continent))
                .ForMember(c => c.Language, o => o.MapFrom(s => s.Language))
                .ForMember(c => c.Culture, o => o.MapFrom(s => s.Culture));
            CreateMap<AuthorModel, Author>()
                .ForMember(c => c.Name, o => o.MapFrom(s => s.Name))
                .ForMember(c => c.Surname, o => o.MapFrom(s => s.Surname))
                .ForMember(c => c.Email, o => o.MapFrom(s => s.Email))
                .ForMember(c => c.Country, o => o.MapFrom(s => s.Country))
                .ForMember(c => c.City, o => o.MapFrom(s => s.City))
                .ForMember(c => c.ImageUrl, o => o.MapFrom(s => s.ImageUrl));

            //Update
            CreateMap<RecipeModel, Recipe>();
            CreateMap<RecipeStepsModel, RecipeStep>()
                .ForMember(c => c.Instruction, o=> o.MapFrom(s => s.Instruction))
                .ForMember(c => c.ImageUrl, o => o.MapFrom(s => s.ImageUrl));
            CreateMap<RecipeIngredientModel, RecipeIngredient>();
            CreateMap<IngredientModel, Ingredient>();
            CreateMap<IngredientQuantityModel, IngredientQuantity>()
               .ForMember(mu => mu.Amount, mf => mf.MapFrom(u => u.Amount))
               .ForPath(mu => mu.MeasurementUnit.Unit, mf => mf.MapFrom(u => u.MeasurementUnit));

            //GetAll
            CreateMap<Recipe, RecipeModel>();
            CreateMap<Author, AuthorModel>();
            CreateMap<RecipeStep, RecipeStepsModel>();
            CreateMap<Country, CountryModel>();
            CreateMap<RecipeIngredient, RecipeIngredientModel>();
            CreateMap<Ingredient, IngredientModel>();
            CreateMap<IngredientQuantity, IngredientQuantityModel>()
                .ForMember(mu => mu.MeasurementUnit, mf => mf.MapFrom(u => u.MeasurementUnit.Unit));
        }
    }
}
