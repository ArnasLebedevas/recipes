﻿using Recipes.Domain;
using Recipes.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recipes.Data.Core.Base
{
    public abstract class BaseRepository<TEntity, TModel> : IRepository<TEntity, TModel>
        where TModel : class, IEntityModel
        where TEntity : class, IEntity
    {
        protected IWriteRepository _repository;

        public BaseRepository(IWriteRepository repository)
        {
            _repository = repository;
        }

        public abstract Task<int> Add(TModel entity);

        public abstract Task<TEntity> Delete(int id);

        public abstract Task<List<TModel>> GetAllAsync();

        public abstract Task<TModel> GetAsync(int id);

        public abstract Task<TModel> Update(TModel entity);
    }
}
