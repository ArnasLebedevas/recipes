﻿using Recipes.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Recipes.Data.Core.Base
{
    public interface IWriteRepository
    {
        void Add<TEntity>(TEntity entity) where TEntity : class;

        void Delete<TEntity>(TEntity entity) where TEntity : class;

        Task<TEntity> FirstOrDefaultAsync<TEntity>(int id) where TEntity : class, IEntity;

        Task<List<TEntity>> GetAllAsync<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class, new();

        Task<TEntity> FirstOrDefaultAsync<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class, new();

        Task SaveChangesAsync();

        IQueryable<TEntity> Query<TEntity>() where TEntity : class;
    }
}
