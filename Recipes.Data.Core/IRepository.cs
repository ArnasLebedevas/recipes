﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recipes.Data.Core
{
    public interface IRepository<TEntity, TModel>
    {
        Task<List<TModel>> GetAllAsync();
        Task<TModel> GetAsync(int id);
        Task<int> Add(TModel entity);
        Task<TModel> Update(TModel entity);
        Task<TEntity> Delete(int id);
    }
}
